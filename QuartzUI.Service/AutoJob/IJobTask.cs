﻿using System.Threading.Tasks;
using QuartzUI.Code;

namespace QuartzUI.Service.AutoJob
{
    public interface IJobTask
    {
        //执行方法
        Task<AlwaysResult> Start();
    }
}
