using System;
using System.Collections.Generic;
using System.Text;

namespace QuartzUI.Code.Model
{
    public class SystemConfig
    {
        /// <summary>
        /// 是否是Demo模式
        /// </summary>
        public bool Demo { get; set; } = false;
        /// <summary>
        /// 是否是调试模式
        /// </summary>
        public bool Debug { get; set; }
        /// <summary>
        /// 允许一个用户在多个电脑同时登录
        /// </summary>
        public bool LoginMultiple { get; set; } = true;
        public string LoginProvider { get; set; } = "Cookie";
        /// <summary>
        ///  数据库超时间（秒）
        /// </summary>
        public int CommandTimeout { get; set; } = 180;
        /// <summary>
        /// 允许跨域的站点
        /// </summary>
        public string AllowCorsSite { get; set; }
        /// <summary>
        /// 主程序数据库编号
        /// </summary>
        public string MainDbNumber { get; set; }
        public string DBProvider { get; set; }
        public string DBConnectionString { get; set; }

        public string CacheProvider { get; set; } = "Memory";
        public string RedisConnectionString { get; set; } = "127.0.0.1:6379";
        public string TokenName { get; set; } = "BZ-Token";
        //缓存过期时间
        public int LoginExpire { get; set; } = 1;
        public bool? LocalLAN { get; set; } = true;
        /// <summary>
        /// 项目前缀
        /// </summary>
        public string ProjectPrefix { get; set; } = "QuartzUI";
        /// <summary>
        /// 是否重置密码
        /// </summary>
        public bool? ReviseSystem { get; set; }
        public string RevisePassword { get; set; }
        /// <summary>
        /// 多数据库组
        /// </summary>
		public List<DBConfig> SqlConfig { get; set; }
        /// <summary>
        /// 是否集群
        /// </summary>
        public bool? IsCluster { get; set; }
        /// <summary>
        /// 是否删除定时调度任务
        /// </summary>
        public bool? NeedClear { get; set; }
        /// <summary>
        /// 是否开启定时任务
        /// </summary>
        public bool? OpenQuartz { get; set; }
        public DocumentSettings DocumentSettings { get; set; }
        public MqConfig RabbitMq { get; set; }
        public string HomePage { get; set; } = "/SystemSecurity/OpenJobs/Index";

    }
    public class DocumentSettings
    {
        public string DocumentTitle { get; set; }
        public List<GroupOpenApiInfo> GroupOpenApiInfos { get; set; }
    }
    public class GroupOpenApiInfo
    {
        public string Group { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
    }
    public class DBConfig
    {
        /// <summary>
        /// 数据库序号
        /// </summary>
		public string DBNumber { get; set; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public string DBProvider { get; set; }
        /// <summary>
        /// 数据库连接
        /// </summary>
        public string DBConnectionString { get; set; }
    }
}
