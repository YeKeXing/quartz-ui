﻿/*******************************************************************************
 * Copyright © 2016 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/

namespace QuartzUI.Code
{
    public class TreeSelectModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public string code { get; set; }
        public string parentId { get; set; }
        public object data { get; set; }
    }
}
