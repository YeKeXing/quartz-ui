﻿/*******************************************************************************
 * Copyright © 2016 QuartzUI.Framework 版权所有
 * Author: QuartzUI
 * Description: QuartzUI快速开发平台
 * Website：
*********************************************************************************/

using System.Collections.Generic;

namespace QuartzUI.Code
{
    public class TreeGridModel
    {
        public string id { get; set; }
        public string parentId { get; set; }
        public string title { get; set; }
        public object self { get; set; }
        public object checkArr { get; set; }
        public bool? disabled { get; set; }
        public List<TreeGridModel> children { get; set; }
    }
}
